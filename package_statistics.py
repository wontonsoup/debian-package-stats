#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import itertools
import re
import requests
import urllib.parse
import zlib

CHUNK_SIZE = 32 * 1024

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('arch', nargs=1, type=str,
                        help='look up packages for architecture ARCH',
                        metavar='ARCH')
    parser.add_argument('-m', '--mirror',
                        default='http://ftp.uk.debian.org/debian/dists/stable/main/',
                        type=str, help='mirror to use')
    parser.add_argument('-n', default=10, type=int,
                        help='display top N packages', metavar='N')

    args = parser.parse_args()

    uri = urllib.parse.urljoin(args.mirror, 'Contents-{}.gz'.format(args.arch[0]))
    # 16 for handling gzip.
    # https://www.zlib.net/manual.html#inflateInit2
    dobj = zlib.decompressobj(wbits=zlib.MAX_WBITS | 16)
    counts: dict[bytes, int] = {}

    with requests.get(uri, stream=True) as resp:
        resp.raise_for_status()

        # If chunk contains a truncated line, we stash it and re-parse it with
        # additional data read.
        trailer = bytes()

        # Streaming the response for extra vroomage.
        for chunk in resp.iter_content(chunk_size=CHUNK_SIZE):
            d = trailer + dobj.decompress(chunk)
            lines = d.split(b'\n')
            if not lines[-1].endswith(b'\n'):
                trailer = lines.pop()
            for line in lines:
                pair = line.rsplit(b' ')
                file = pair[0].strip()
                packages = pair[-1]
                for package in packages.split(b','):
                    # [[$AREA/]$SECTION/]$NAME
                    # We only care about the name.
                    name = package.rsplit(b'/', maxsplit=1)[-1]
                    counts[name] = counts.get(name, 0) + 1

    counts = dict(sorted(counts.items(), key=lambda item: item[1], reverse=True))
    for package, count in itertools.islice(counts.items(), args.n):
        # Random alignment, might break. :^)
        print('* {:<32} {:>8}'.format(package.decode('utf-8'), count))
